#
# set up global data file names
#
# parameter:
#   KEY <value> e.g. de or sepa
#   PREFIX <prefix> prefix for generating filename which is build from <prefix>_%Y%m%d.txt
#
macro(setup_filenames)
    set(options)
    set(oneValueArgs KEY PREFIX)
    set(multiValueArgs)
    cmake_parse_arguments(_ "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )
    if(ENABLE_BANKDATA_DOWNLOAD)
        string(TIMESTAMP _DATA_FILE "${__PREFIX}_%Y%m%d.txt")
        set(DATA_FILE ${_DATA_FILE})
        set(DATA_FILEPATH ${CMAKE_CURRENT_SOURCE_DIR}/${DATA_FILE})
        message(STATUS "Using data file ${DATA_FILEPATH} for key ${__KEY}")
    else()
        file(GLOB DATA_FILEPATHS "${CMAKE_CURRENT_SOURCE_DIR}/${__PREFIX}_2*.txt")
        list(SORT DATA_FILEPATHS COMPARE FILE_BASENAME)
        list(POP_BACK DATA_FILEPATHS DATA_FILEPATH)
        get_filename_component(DATA_FILE ${DATA_FILEPATH} NAME)
        message(STATUS "Using present data file '${DATA_FILEPATH}' for key ${__KEY}")
    endif()
    set(DATA_FILEPATH_${__KEY} ${DATA_FILEPATH}  CACHE STRING "raw data file path" FORCE)
endmacro()

#
# download a file from the specified URL
#
# parameter:
#   KEY <value> e.g. SEPA
#   URL <url> url for data file
#
# result:
#   The downloaded file is accessable with ${DATA_FILEPATH_<KEY>}
#
macro(fetch_file)
    set(options)
    set(oneValueArgs KEY URL)
    set(multiValueArgs)
    cmake_parse_arguments(_ "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )
    setup_filenames(
        KEY ${__KEY}
        PREFIX ${__KEY}
    )

    if(ENABLE_BANKDATA_DOWNLOAD)
        # generate cmake script for downloading bank data file
        file(WRITE ${CMAKE_CURRENT_BINARY_DIR}/tmp.cmake "
            set(FILE_URL \"${__URL}\")
            file(DOWNLOAD \"\${FILE_URL}\" \"${DATA_FILEPATH_${__KEY}}\")
        ")

        # fetch raw bankdata file
        add_custom_command(
            OUTPUT ${DATA_FILEPATH_${__KEY}}
            COMMAND ${CMAKE_COMMAND} -P ${CMAKE_CURRENT_BINARY_DIR}/tmp.cmake
            WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
            COMMENT "Download raw data file to ${DATA_FILEPATH_${__KEY}}"
        )
    endif()

    add_custom_target(fetch_${__KEY}
        DEPENDS ${DATA_FILEPATH_${__KEY}}
    )
endmacro()

#
# add method tests for ktoblzcheck library
#
# The macro add add tests

# parameter:
#   <method> the method to add the test e.g. 45
#   PASS <account[s]> list of space separated accounts, which should pass the test
#   FAIL <account[s]> list of space separated accounts, which should fail
#
macro(add_method_tests method)
    set(options)
   set(oneValueArgs )
   set(multiValueArgs PASS FAIL)
   cmake_parse_arguments(ARGS "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN})

   list(REMOVE_DUPLICATES ARGS_PASS)
   foreach(account ${ARGS_PASS})
        add_test(
            NAME ktoblzcheck_method_pass_${method}_${account}
            COMMAND ${TEST_WRAPPER} $<TARGET_FILE:ktoblzcheck-bin> --file=${BANKDATA_TEST_DBPATH} 000000${method} ${account}
        )
    endforeach()
    list(REMOVE_DUPLICATES ARGS_FAIL)
    foreach(account ${ARGS_FAIL})
        # a test can't pass and fail
        if(NOT account IN_LIST ARGS_PASS)
            add_test(
                NAME ktoblzcheck_method_fail_${method}_${account}
                COMMAND ${TEST_WRAPPER} $<TARGET_FILE:ktoblzcheck-bin> --file=${BANKDATA_TEST_DBPATH} 000000${method} ${account}
            )
            set_tests_properties(ktoblzcheck_method_fail_${method}_${account} PROPERTIES WILL_FAIL TRUE)
        endif()
    endforeach()
endmacro()
