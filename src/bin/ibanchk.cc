#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "iban.h"

#include <iostream>
using namespace std;

#include <string.h>

void printUsage()
{
    cout << "Usage:\n"
         << "ibanchk [<options>] iban\n"
         << "  --help, -h:             This help message\n"
         << "  --version, -v:          Print the version number and exit.\n"
         << "  --file=<ibandata-file>: Use this specified file as iban database\n"
    ;
}

int main(int argc, char *argv[])
{
    if (argc < 2) {
        printUsage();
        return 2;
    }
    if (argc == 2 && (strcmp(argv[1], "-h") == 0 || strcmp(argv[1], "--help") == 0)) {
        printUsage();
        return 1;
    } else if (argc == 2 && (strcmp(argv[1], "-v") == 0 || strcmp(argv[1], "--version") == 0)) {
        cout << VERSION << endl;
        return 0;
    }

    const char *arg = "--file";
    std::string bankdataFile;
    std::string ibanArg;
    if (argc == 3 && strstr(argv[1], arg)) {
        bankdataFile = argv[1]+strlen(arg)+1;
        ibanArg = argv[2];
    } else if (argc == 2 && *argv[1] != '-') {
        ibanArg = argv[1];
    } else {
        printUsage();
        return 2;
    }

    IbanCheck ibanchk(bankdataFile);
    if (ibanchk.error()) {
        std::cerr << "Fehler beim Lesen von ibandata.txt" << std::endl;
        return 3;
    }
    Iban iban(ibanArg);
    IbanCheck::Result res = ibanchk.check(iban);
    std::cout << iban.printableForm() << ": "
              << ibanchk.resultText(res) << std::endl;
    if (res == IbanCheck::OK) {
        return 0;
    } else {
        return 1;
    }
}
